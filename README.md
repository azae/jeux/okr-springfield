https://gitlab.com/azae/jeux/okr-springfield/

# Reste à faire

* fiche / expliquer tour de consentement
* fiche conclusion
* cartes événements

# Des OKR à Springfield

## Objectif

Ce serious game a pour objectifs de faire comprendre les OKR, son vocabulaire et sa mise en pratique dans le temps.

Dans l'ordre de priorité : 
* Comment construire des OKRs tactiques
* principe de l'alignement vertical
* principe de l'alignement horizontal
* le suivi mensuel
* le suivi trimestriel
* présenter un exemple de board associé.

# Préparation

* Imprimer les boards pour chaque équipe (suivi des OKR) [board.pdf](./board.svg.pdf)
* Imprimer le context pour chaque équipe, si possible en A3 [context.pdf](./context.svg.pdf)
* Imprimer les cartes événements (TODO lien pdf)
* Imprimer les fiches (TODO liens) 
* 3 dés 6

# OKR 

Voir la [fiche OKR](./fiches/okr.svg.pdf)
Voir la [fiche choix des OKR](./fiches/choix-okr.svg.pdf)

# Liste des services

## Entretien

![](./services/entretien.svg.png)

## Finances

![](./services/finances.svg.png)


## Communication

![](./services/communication.svg.png)


## Informatique

![](./services/informatique.svg.png)

## Éducation - Culture - Sport

![](./services/education.svg.png)

## État civil - cimetière

![](./services/etat-civil.svg.png)

## Sécurité - Police

![](./services/securite.svg.png)

## Santé

![](./services/sante.svg.png)


## Resources Humaines

![](./services/rh.svg.png)

# Déroulé presque 3h.

| Groupe | Temps | Description |
|--------|-------|-------------|
| Grand groupe | 10 - 15 min | pour présenter le contexte, l'ambiance de la ville, qui est le maire et répartir les gens dans les services |
| Petits groupes | 10 min | Incarner son équipe et trouver les 2 ou 3 grands axes de travail de l'équipe ou du service, les 2 ou 3 grands impacts  que vous voulez obtenir et comment y arriver. |
| Grand groupe | 20 min | partage des axes (1min par équipe) + point sur alignement verticale et motivation / embarquement des équipes |
| Petits groupes | 10 min | Corriger les axes et produire les 2 ou 3 "Objectifs tactiques" avec la fiche |Objectif tactique et l'aide des coachs
| Grand groupe | 30 min | tour de consentement : chaque équipe présente ses objectifs en 1 ou 2 min, puis on demande à toutes les autres équipes leur consentement (non opposition) |
| Petits groupes | 10 min | Trouver les 1 ou 2 Keys Results associés à chaque objectif, avec la fiche KR tactique (entre 3 et 5 KR au total) |
| Grand groupe | 20 min | partage des OKR (1min par équipe) + échange sur alignement horizontal cf. tour de consentement |

Il est possible de s'arrêter là pour avoir les 3 principaux apports pédagogiques. Dans ce cas, prévoir une séquence de cloture genre 2-4-tous ou batton de parole.

| Groupe | Temps | Description |
|--------|-------|-------------|
| Grand groupe | 5 min | fin du premier mois (tirage de la carte événement + tirage des dés) |
| Petits groupes | 5 min | répartition de l'avancement des KR |
| Grand groupe | 10 min | tour de partage et de transparence horizontal, en cas de dépendances entre OKR entres différentes équipes, les équipe sont invités à réaligner leurs pourcentages. |
| Grand groupe | 5 min | fin du psecond mois (tirage de la carte événement + tirage des dés) |
| Petits groupes | 5 min | répartition de l'avancement des KR |
| Grand groupe | 10 min | tour de partage et de transparence horizontal |
| Grand groupe | 5 min | fin du troisième mois (tirage de la carte événement + tirage des dés) |
| Petits groupes | 5 min | répartition de l'avancement des KR |
| Grand groupe | 5 min | Annonce et partage des mesures des OKR stratégiques |
| Grand groupe | 10 min | tour de partage et de transparence horizontal |
| Grand groupe | 10 min | présentation de ce qui ce passe ensuite. (définition des OKR tactique pour le trimestre suivant, tour de consentement, etc.)|
| Groupes (2-4-tous) | 3×5min | fiche conclusion | 

## Lancement du jeu

Voir [l'intro](./intro.svg.pdf) 

Puis distribuer à chaque équipe sa fiche équipe et le [A3 de contexte](./context.svg.pdf)

## Déroulé d'une fin de mois

Voir le fichier [rules.pdf](./rules.pdf)

## Déroulé d'une fin de trimestre

Voir le fichier [rules.pdf](./rules.pdf)

Note pour les animateurs :

* le nomnbre d'arbre remplacé (le chiffre doit être improvisé par le facilitateur en fonction du jeu et de l'avancement des OKR tactiques)
* On pourrait aussi souligner que le niveau dans les sondages baisse, et qu'il faut faire quelques chose ... Ça relancerait le débat sur les objectifs implicite et la necessité pour la direction d'expliciter tous ses OKR stratégiques. Mais dans la dynamique c'est démotivant.

# Liste des événements

## Événements

1. C'est l'arbre qui cache la forêt, l'avancement réel de vos OKR est de 10% × nombre d'OKR.
1. Homer simpson s'est présenté au élections, il veut remplacer tous les animaux par des animaux en plastiques. Ça prend du temps mais les OKRs ne sont pas impactés.
1. TODO histoire : Les OKRs des équipes ne peuvent avancer que si les OKRs de l'équipe finances dépasse 50%.
1. TODO Histoire : Les OKRs des équipes ne peuvent avancer que si un des OKRs de l'équipe entretien dépasse 50%.
1. Homer simpson a reçu un colis de chine qui contenait la grippe OZ ce qui a contaminée tout Springfield, aucun OKR n'avance ce moi.
1. Suite à la révélation #MeToo de Lisa, tous les hommes de Springfield sont obligé de se faire référencé comme délinquants sexuels, les OKR n'avanceront que de la moitier ce moi.
1. Willie le jardinier à attiré tous les rats de Springfield dans le centre commeriale provocant une panique générale, les rats affamés ont mangé une partie des arbres en plastiques. L'avancement de vos OKR reculent des dés avancement.
1. Les SDF ont fait brulé tout le parc par inadvertance, toute la mairie est mise à contribution pour réparer, aucun OKR n'avance ce mois mais le taux de remplacement des arbres bondi de 15%.
1. Homer en se battant avec le lapin de Pâques a provoqué un accident nucléaire, l'équipe communication gère la crise et ses OKR n'avance que de moitier.
1. En jouant dans la chaufferie municipale, Bart a déclanché l'explosion de tous les radiateurs des lieux publiques, aucune incidence sur les OKR.
1. En voulant extorquer une rançon à M. Burns, Bart et ses copains lachent un cryptolocker sur les ordinateurs de la mairie. Les OKR du service informatique n'avance que de moitier le temps de gérer la crise.
1. C'est la course de canards annuelle à Springfield toute la mairie y participe, la motivation explose, la capacité d'avancement de toutes les équipes de ce moi augmente de 50%.
1. Homer a détruit accidentellement le boulevard aux fast-food, comme il faut gérer la crise les OKR n'avance que de moitier.
1. Les déchets nucléaires de la centrale de M. Burns sont produits en trop grande quantité. Des vapeurs toxiques s’en échappe et la ville est évacuée durant 1 semaine pour régler le problème. Les OKR n’avancent que de moitier ce moi ci.
1. Le professeur Frink est un scientifique ringard, extrêmement intelligent mais fou. Il cré d’étranges inventions pour aider la ville même si elles ne font qu’empirer les choses. Afin d’aider le service entretient de la mairie, il créer le robot nettoyeur. Cependant le Robot n’en fait qu’a sa tête et décide de détruire tout un quartier de Springfield. Les OKR de l'équipe qui vient de tirer cette carte sont stopé, mais le nombre d'arbres remplacés est augmenté de 10%
1. La maison de retraite de Springfield est un endroit isolé, délabré où les patients se sentent seuls et déprimés. Les retraités et le personnel décident de manifester pour redorer l’endroit. Les OKR de l'équipe Santé n’avancent que de moitié le temps de gérer la crise.
1. Otto, le chauffeur du bus scolaire a trop bu le lundi matin et provoque un accident et meur. Les OKR de l'équipe RH n’avancent que de moitié le temps de recruter un nouveau chauffeur de bus. 
1. Doris Freedman, La cuisinière de l’école ne respecte pas la moindre mesure d’hygiène ce qui provoque une intoxication alimentaire générale. Les OKR de l'équipe Éducation n'avancent que de moitié.
1. Tahiti Bob, ancien partenaire de Krusty le clown tombé dans le crime, tente un coup d’état avec quelques fidèles afin d’avoir assez de pouvoir pour ruiner la vie de Bart. Une bagarre générale éclate dans la ville. Les OKR de l'équipe Sécurité n’avancent que de moitié.
1. La secte des adorateurs de serpents de Springfield laisses s'échapper tous leurs amis reptiliens lors lors d’une scéance de présentation dans la mairie ce qui créer la panique générale. Les OKRs de l'équipe état civil n'avance que de moitier le temps de tous les capturer.

# Débrieffing

* KR implicite
* dépendance entre KR
* KR mesurable

# Fiche Conclusion :

Afin de conclure le serious game, quelques questions sont posées aux différents participants. Ces questions permettent à chacun de faire une petite retrospective sur ce qui a été vu, intégré et adopté lors de ce serious game. Chacun écrit un petit post-it pour répondre à ces différentes questions (5mins)

1. Qu'est ce que j'ai appris durant cette activité ?  
2. Quels sont les outils et améliorations avec lesquels je repars ?  
3. Qu'est ce que je pourrais essayer de mettre en oeuvre demain ? Comment est-ce que je compte faire, mes premières actions ? 

SVG_FILES = $(shell find . -type f -name '*.svg' ! -path "./TheSimpsons/*" )
MD_FILES = $(shell find . -type f -name '*.md')

%.svg.pdf: %.svg
	inkscape --export-type="pdf" --export-filename="$@" $<

%.svg.png: %.svg
	inkscape --export-type="png" --export-filename="$@" $<

%.md.html: %.md
	pandoc -s "$<" -c okr.css --template template.html -o "$@"

%.md.png: %.md.html
	chromium --headless --disable-gpu --no-sandbox --screenshot=$@ "file://$(shell pwd)/$<"

svg2pdf: $(patsubst %.svg,%.svg.pdf,$(SVG_FILES))

svg2png: $(patsubst %.svg,%.svg.png,$(SVG_FILES))

md2html: $(patsubst %.md,%.md.html,$(MD_FILES))

md2png: $(patsubst %.md,%.md.png,$(MD_FILES))

all: md2png svg2png md2html svg2pdf 

ci:
	inotify-hookable -f Makefile $(patsubst %, -f %, $(SVG_FILES)) $(patsubst %, -f %, $(MD_FILES)) -c "make"

